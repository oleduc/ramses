# `Ramses`

[![Build Status](https://travis-ci.org/brandicted/ramses.svg?branch=master)](https://travis-ci.org/brandicted/ramses)
[![Documentation](https://readthedocs.org/projects/ramses/badge/?version=stable)](http://ramses.readthedocs.org)
[![Join the chat at https://gitter.im/brandicted/ramses](https://img.shields.io/badge/gitter-join%20chat%20%E2%86%92-brightgreen.svg)](https://gitter.im/brandicted/ramses)

Generate a RESTful API using [RAML](http://raml.org). It uses Pyramid and [Nefertari](https://github.com/brandicted/nefertari) which provides Elasticsearch / Posgres / MongoDB -powered views.

Looking to get started quickly? You can take a look at the ["Getting Started" guide](https://ramses.readthedocs.org/en/stable/getting_started.html).
